import * as React from "react";

import {RouteComponentProps} from "react-router-dom";
import api from "../api"
import { PostPage } from "./IndexPage";
import { withPosts } from "./IndexPage";





async function init(tagChain:string) {
    let tags = tagChain.split(",").map(t=> t.trim()).filter(Boolean);
    return api.getPostsByTags(tags);
}

export interface ITagsPageProps {
    tags:string;
}

export const TagsPage = ({match}: RouteComponentProps<ITagsPageProps>) => {
    const Content = withPosts(PostPage, init.bind(null, match.params.tags))

    return <Content/>
}

// export default TagsPage;