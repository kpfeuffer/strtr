import * as React from "react";
import {RouteComponentProps} from "react-router-dom";
import { EditPostForm } from "../layout/components";
import { BlogPost } from "client/types";
import api from "../api";

export interface INewPostPageProps extends RouteComponentProps<{}>{

}

export function NewPostPage ({history}:INewPostPageProps) {
    return (<EditPostForm initialPost={null} onSave={async form=> {
        const tags = form.tags.filter(Boolean);
        const now = new Date();
        const post: BlogPost = {
            ...form,
            tags:tags,
            id:now.getTime(),
            created:now
        }

        await api.addBlogPost(post);

        history.push("/blog/"+post.id);
    }
    }/>);
}