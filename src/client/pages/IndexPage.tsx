import * as React from "react";
import {useState, useEffect} from "react";
import api from "../api"
import { BlogPostItem } from "../layout/components";
import { BlogPost } from "../types";

async function init() {
    // const dataSource:BlogPost[] = [{
    //     id:123,
    //     content:"# Content",
    //     title:"Test Post",
    //     created: new Date(),
    //     tags: ["sharepoint", "javascript"]
    // }, {
    //     id:345,
    //     content:"# Markdown \n## Rockt \n### die \n#### Hütte",
    //     title:"Test Post",
    //     created: new Date(),
    //     tags: ["sharepoint", "javascript"]
    // }];
    // for (const post of dataSource) {
    //     await api.addBlogPost(post);
    // }
    return api.getAllBlogPosts();
}

// export function IndexPage() {
//     const [posts, setPosts] = useState(0 as number | BlogPost[]);
    
//     useEffect(()=> {
//         init().then((posts)=>{
//             setPosts(posts);
//         });
//     });
//     return (<>
//         <div>
//             {typeof posts == "object" ? posts.map((post: BlogPost, i:number)=> 
//                 <BlogPostItem post={post} key={i}/>

//             ) : <h1>Loading</h1>}
//         </div>
//     </>);
// }

export function PostPage({init}: {init:()=> Promise<BlogPost[]>}) {
    const [posts, setPosts] = useState(0 as number | BlogPost[]);
    
    useEffect(()=> {    
        init().then((posts)=>{
            setPosts(posts);
        });
    });
    return (<>
        <div>
            {typeof posts == "object" ? posts.map((post: BlogPost, i:number)=> 
                <BlogPostItem post={post} key={i}/>

            ) : <h1>Loading</h1>}
        </div>
    </>);

}

export const withPosts = function (WrappedComponent: any, dataCallback: ()=> Promise<BlogPost[]>) {
    return function() {
        return (<WrappedComponent init={dataCallback}/>)
    }
}

export const IndexPage = withPosts(PostPage, init);
// export default IndexPage;