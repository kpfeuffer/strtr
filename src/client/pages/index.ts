// import {lazy} from "react";

// export const IndexPage = lazy(()=> import("./IndexPage"));
export * from "./IndexPage";
// export const SinglePostPage = lazy(()=> import("./SinglePostPage"))
export * from "./SinglePostPage";
// export const NewPostPage = lazy(()=> import("./NewPostPage"));
export * from "./NewPostPage";
// export const EditPostPage = lazy(()=> import("./EditPostPage"))
export * from "./EditPostPage";
// export const TagsPage = lazy(()=> import("./TagsPage"))
export * from "./TagsPage";