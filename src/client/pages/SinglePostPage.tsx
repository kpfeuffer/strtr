import * as React from "react";
// import {useState, useEffect} from "react";
import api from "../api"
import {RouteComponentProps} from "react-router-dom";
// import { BlogPostItem } from "../layout/components";
// import { BlogPost } from "../types";
import {withPosts, PostPage} from "./IndexPage";
export interface SinglePostPageRouterProps {
    id:string;
}
export interface SinglePostPageProps extends SinglePostPageRouterProps {
   
}

async function load(id:string) {
    let post = api.getSingleBlogPost(+id);
    return post;
}

export function SinglePostPage({match}:RouteComponentProps<SinglePostPageProps>) {
    const Content = withPosts(PostPage, ()=> load(match.params.id).then(res=> [res]));
    return (<Content/>);
}

