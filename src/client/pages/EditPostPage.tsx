import * as React from "react";
import {useState, useEffect} from "react";
import api from "../api"
import {RouteComponentProps} from "react-router-dom";
// import { BlogPostItem } from "../layout/components";
// import { BlogPost } from "../types";
import { EditPostForm, IEditPostFormState } from "../layout/components";
import { BlogPost } from "../types";
export interface EditPostPageRouterProps {
    id:string;
}
export interface EditPostPageProps extends EditPostPageRouterProps {
   
}

async function load(id:string) {
    let post = api.getSingleBlogPost(+id);
    return post;
}

async function edit(post: BlogPost) {
    return api.updateBlogPost(post);
}

export function EditPostPage({match, history}:RouteComponentProps<EditPostPageProps>) {
    const [currentPost, setPost] = useState(null);

    useEffect(()=> {
        load(match.params.id).then(setPost);
    }, [currentPost]);

    async function onSave(data:IEditPostFormState) {
        const post = {
            ...currentPost,
            ...data
        } as BlogPost;
        console.log(post);
        await edit(post);
        history.push("/blog/"+currentPost.id)
    }

    // const Content = withCreator(EditPostForm, ()=> load(match.params.id).then(res=> [res]));
    return currentPost&&(<EditPostForm 
        initialPost={currentPost}
        onSave={onSave}/>);
}
