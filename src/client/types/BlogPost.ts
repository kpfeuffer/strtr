export interface BlogPost
{
  id: number;
  title: string;
  content: string;
  created: Date;
  tags: string[];
};