import * as React from 'react';

export function AppFooter({text}:{text:JSX.Element}) {
    return (        
    <footer className="mt-auto py-3 blog-footer">
        <p>{text}</p>
        <p>
          <a href="#">Back to top</a>
        </p>
    </footer>
    )
}