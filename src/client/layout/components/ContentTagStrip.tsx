import * as React from "react";
import { Link } from "react-router-dom";

export interface ContentTagStripProperties{
    tags:string[];
};

export function ContentTagStrip(props:ContentTagStripProperties) {
    const items = props.tags.map((tag, i)=> (
            <Link className="badge badge-primary mr-1" key={i} to={`/tags/${encodeURIComponent(tag)}`}>{tag}</Link>
        ));

    return (
        <div>
            {items}
        </div>
    )
}