import {BlogPost} from "../../types"

import * as React from "react";
import * as Showdown from "showdown";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {ContentTagStrip} from "./"
import moment from "moment";

export interface IBlogPostItemProps {
    post: BlogPost
}

const cv = new Showdown.Converter();

export function BlogPostItem ({post}: IBlogPostItemProps){ 
    return post && (<>
    <div className="blog-post">
    <h2 className="blog-post-title">
      <Link to={`/blog/${post.id}`}>{post.title}</Link>
    </h2>
    <Link to={`/blog/${post.id}/edit`} className="btn btn-link btn-sm">
      <FontAwesomeIcon icon="edit" />
    </Link>
    {typeof post.created == "object" && <span className="blog-post-meta">{moment(post.created.toDateString()).format("LLLL")}</span>}
    <ContentTagStrip tags={post.tags}/>
    <div dangerouslySetInnerHTML={{__html:cv.makeHtml(post.content)}}>
    </div>
  </div>
  </>);
}