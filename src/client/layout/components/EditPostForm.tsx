import * as React from "react";
import {useState} from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { BlogPost } from "../../types";


export interface IEditPostFormProps {
	onSave: (data:IEditPostFormState)=> void;
	initialPost:BlogPost;
}

export interface IEditPostFormState {
	title:string; 
	content:string; 
	tags:string[];
}

export function EditPostForm ({onSave, initialPost}:IEditPostFormProps) {
	const[title, setTitle] = useState(initialPost && initialPost.title || "");
	const [content, setContent] = useState(initialPost && initialPost.content || "");
	const [tags, setTags] = useState(initialPost && initialPost.tags || [] as string[]);


	return (<>
	<div className="row mt-3">
		<div className="col-md-12 mb-3">
			<input type="text" className="form-control form-control-lg" placeholder="Title..." value={title} onChange={e=> setTitle(e.target.value)} />
		</div>
	 
		<div className="col-md-12 mb-3">
			<textarea className="form-control blog-content-textarea" placeholder="Blogtext..." value={content} onChange={e=> setContent(e.target.value)}></textarea>
		</div>
	 
		<div className="col-md-12 mb-3">
			<input type="text" className="form-control" placeholder="Tags..." 
			value={tags.join(",")} 
			onChange={e=> setTags(e.target.value.split(","))}/>
		</div>
	</div>
	 
	<div className="row">
		<div className="col-md-12 text-right">
			<button type="button" className="btn btn-success" onClick={onSave.bind(null, {title, content, tags:tags.map(tag=> tag.trim())})}>
				<FontAwesomeIcon icon="save" />&nbsp;Save
			</button>
		</div>
	</div>
	</>);
}