// 1st party
import {AppHeader, AppSidebar, AppFooter} from "./";
import "moment";
// 3rd party
import * as React from 'react';
// import { BlogPostItem } from "./components";
import {Route, Switch} from "react-router-dom";

// const { IndexPage, SinglePostPage, NewPostPage, EditPostPage, TagsPage } = React.lazy(()=> import("../pages"))
import { IndexPage, SinglePostPage, NewPostPage, EditPostPage, TagsPage } from "../pages";

export function App()
{
  return <>
      <AppHeader title="IPI Blog"/>

    <main role="main" className="container">
      <div className="row">
        <div className="col-md-8 blog-main">
          <React.Suspense fallback={<h3>Loading..</h3>}>
            <Switch>
              <Route exact path='/' component={IndexPage}/>
              <Route exact path='/new' component={NewPostPage}/>
              <Route exact path='/blog/:id' component={SinglePostPage}/>
              <Route exact path='/blog/:id/edit' component={EditPostPage}/>
              <Route exact path='/tags/:tags' component={TagsPage}/>
            </Switch>
          </React.Suspense>

        </div>

        <AppSidebar/>
      </div>
    </main>
    <AppFooter text={<>Blog template built for some <a href="https://getbootstrap.com/">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a>.</>}/>
  </>;
};